//получаем арифметический оператор
const getPromptOperator = (message = '') => {
    const operator = prompt(message);
    const validOperators = ['+', '-', '/', '*'];

    //пользователь закрыл всплывающее окно. считаю, что дальнейшее взаимодействие
    // с програмой его не интересует, поэтому выход
    if (operator === null) {
        return;
    }

    if (!validOperators.includes(operator)) {
        alert(`Ошибка! Введенный вами оператор "${operator}" не является валидным`);
        return getPromptOperator(message);
    }

    return operator;
}

//получаем операнд
const getPromptNumber = (message = '') => {
    const userNumber = prompt(message);

    if(userNumber === null) {
        return;
    }

    if (!userNumber.length || isNaN(Number(userNumber))) {
        alert('Вы ввели некоректное число. Попробуйте еще раз');
        return getPromptNumber(message);
    }

    return userNumber;
}

//считаем
const calculatorCalcHandler = (operator, number1, number2) => {
    switch (operator) {
        case "+":
            return number1 + number2;
        case "/":
            return number1 / number2;
        case "*":
            return number1 * number2;
        case "-":
            return number1 - number2;
        default:
            return;
    }
}

//показываем результат
const calculatorViewHandler = () => {
    const operator = getPromptOperator(`Введите оператор:
      + (для сложения),
      / (для деления),
      * (для умножения),
      - (для вычитания)
     `
    );

    if (operator === undefined) {
        return;
    }

    const firstNumber = getPromptNumber('Введите первое число');

    if (firstNumber === undefined) {
        return;
    }

    const secondNumber = getPromptNumber('Введите второе число');

    if (secondNumber === undefined) {
        return;
    }

    const result = calculatorCalcHandler(operator, firstNumber, secondNumber);

    if (isNaN(result)) {
        alert('Вы ввели неверные данные');
        return;
    }

    alert(`Результат: ${result}`);
};

calculatorViewHandler();
